|Name|Link|Platform|Aspects|Idea|Status|
|:---:|:---|:---:|:---:|:---|:---:|
|Kaslab|https://kaslab.nl/blockchain/ |?||||
|Envirio & Marconet|https://www.marconet.eu/ |?|Collateral|netwrok of CCPs|Development|
|D3 ledger|https://d3ledger.com/ |HL Iroha|Interoperability Token OS|"framework for crypto asset management"|PoC with Slovenia & Russian CSDs|
|WorldBank bond-i Issuance|https://www.worldbank.org/en/news/press-release/2018/08/23/world-bank-prices-first-global-blockchain-bond-raising-a110-million |?|Issuance||PoC for issuance|
|DTCC Blockchain on Repo and Derivatives|http://www.dtcc.com/blockchain |HLF Corda|Performance Live||PoC & experimentation|
|CLSNet|https://www.cls-group.com/products/processing/clsnet/ |HLF| Marketplace Process efficiency||Live|
|ASX and CHESS replacement (Australia)|https://asxchessreplacement.atlassian.net/wiki/spaces/CSP/overview |Digital Assets|Live||Replace the existing CHESS system with a DLT alternative|
|HQLAx with Deutsche Börse Group|https://www.hqla-x.com/	|Corda|Collateral mobilization|Liquidity pooling|Live|
|FundsDLT|https://www.fundsdlt.net/ |Quorum|Straight-through|||
|SIX Group|https://www.six-group.com/securities-services/de/home.html |Corda|Settlement Custody||in Development|
|Societe Generale cevered bond issuance|https://www.societegenerale.com/en/newsroom/first-covered-bond-as-a-security-token-on-a-public-blockchain |Ethereum|Issuance|One shot|
|Santander issuance bond|https://www.finyear.com/Santander-launches-the-first-end-to-end-blockchain-bond_a41441.html|Ethereum|Issuance|One shot|
|ID2S|http://id2s.eu/ | SETL.io|CSD|Operational|
|KDPW (Poland) by IBM|https://www.ibm.com/blogs/think/2017/10/poland-blockchain/ |HLF|Voting|Unclear|
|SBERBank in Russia|https://www.sberbank.com/news-and-media/press-releases/article?newsID=bfbc4479-6a04-46f2-9d52-0b81093590aa&blockID=7&regionID=77&lang=en&type=NEWS |HLF|DvP OS OTC||PoC|PoC	With NSD and more|
|Hong Kong Stock Exchange partnership|https://www.hkex.com.hk/?sc_lang=en |Digital Asset|Settlement||partnership|
|Japan Exchange Group PoCs|https://www.jpx.co.jp/english/corporate/news/news-releases/0010/20161130-01.html |HLF|Structure||Consortium|
|Strate for eVoting|https://www.strate.co.za/e-voting |?|Voting||Unclear|
|Borsa Y Mercado Espanoles & Renta 4 Banco test collateral announcement||?|Digititalisation||Expected go live|
|BaNCS Network with Kuwait and  Canada|https://www.tcs.com/tcs-powers-world-first-cross-border-securities-settlement-between-two-central-depositories-using-quartz-blockchain#section_1 |Quartz|Custody Multi-industry||Unclear|
|Investor Communication in Japan for e-Voting report|https://www.broadridge.com/intl/press-release/2019/icj-and-broadridge-execute-the-proxy-voting-process |?|Voting|Enable proxy voting in Japan|Unclear|
|Santiago Exchange on Blockchain|https://static.sched.com/hosted_files/hgf18/4e/Hyperledger%20Global%20Forum%20-%20Case%20Study%20BCS%20ShortSales%20Lending%20Repository.pdf |HLF|Case Study||
|Value on Chain|http://valueonchain.tech/ |R3|Non-listed||Development|
|LiquidShare.io|https://www.liquidshare.io/ |Ethereum|Non-listed|Development|
|Singapore Exchange for security token platform||Ethereum|Tokenization||Announced|
|India stock exchange for automation and resiliency by IBM|https://www.ibm.com/case-studies/national-stock-exchange-india |HLF|Resiliency Automation||
|Stock Exchange of Thailand (SET) Capital market platform|https://www.set.or.th/dat/news/201805/18040992.pdf |?|Digititalisation||Announced|
|UBIN by Monetary Authority Singapore (MAS)|https://www.mas.gov.sg/schemes-and-initiatives/Project-Ubin |Corda HLF Quorum|Interoperability Privacy Tokenization||PoC
|Jasper by Bank of Canada & al|https://www.bankofcanada.ca/research/digital-currencies-and-fintech/fintech-experiments-and-projects/ |Corda|Interoperability Optimization Tokenization||
|Stella by ECB & BoJ||Element Corda HLF|Performance Interoperability||Research|
|SAP and R3 ledger connection Hybrid ledger||Corda|Interoperability Tokenization||
|BLOCKBASTER|https://www.bundesbank.de/resource/blob/766672/29feab3f9079540441e3abda1ed2d2c1/mL/2018-10-25-blockbaster-final-report-data.pdf| HLF | Performance Settlement | | PoC |
|Qwant network|https://www.quant.network|Java|Interoperability|Connection layer above other ledger|PoC|
|Eurex Clearing AG and Commerzbank|https://www.deutsche-boerse.com/dbg-en/media/press-releases/Commerzbank-Deutsche-B-rse-and-MEAG-to-reach-further-step-in-post-trade-services-using-distributed-ledger-technology--1631510|?|Tokenization |Proof-of-concept|
|RegistrAccess|https://www.ailancy.com/wp-content/uploads/2019/09/slib_cp2_registraccess_12sept2019_vf.pdf|?|?|Register of access|In development| 
|USC|?|?|Tokenisation|tokenise RTGS funds|Ongoing|





